<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\Badges;
use app\models\BillingHistory;
use app\models\CollectionClasses;
use app\models\Collections;
use app\models\UserBadges;
use app\models\UserFavorites;
use app\models\UserFollowingTeachers;
use app\models\UserKarmaLevel;
use app\models\UserWatched;
use app\models\UserWatchStat;
use app\models\UserWatchSummary;
use app\models\UserWishlist;
use app\models\YogaClasses;
use app\models\YogaClasses;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use yii\data\ActiveDataProvider;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\web\Response;
use dektrium\user\models\Profile;
use dektrium\user\models\User;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');



class ProfileController extends ActiveController
{

    public $modelClass = '\dektrium\user\models\Profile';



    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    public function actionIndex(){
        throw new YogaHttpException(403,"You don't have permission");
    }


    /**
     * @api {get} /profile/get_profile get profile
     * @apiDescription Return user profile by access-token
     *
     * @apiName profile_get_profile
     * @apiGroup Profile
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [user_id]  User ID for which you want to get the profile information. If this parameter is not
     * specified - the profile information will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "geo,statistic"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_profile
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_profile($user_id = -1)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");

        if ($user_id == -1){
            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        $cur_user =  User::findOne(['id' => $user_id]);

        if (!isset($cur_user)){
            throw new YogaHttpException(404,"User not found");
        }

        return  $cur_user->profile;
    }



    /**
     * @api {get} /profile/get_watch_later_by_page get watch later classes by page
     * @apiDescription Get user watch later classes
     *
     * @apiName profile_get_watch_later_by_page
     * @apiGroup Profile
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "style,goals"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_watch_later_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watch_later_by_page($page=1, $full_list=0, $page_size=10, $user_id = -1)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($user_id, "user_id");


        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }



        //get watch later list
        $objects_list = UserWishlist::find()->where("user_id=".$user_id)->all();
        $objects_id_array = [];
        foreach($objects_list as $obj){
            $objects_id_array[] = $obj->class_id;
        }

        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->limit($page_size*$page);
        }
        else{

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /profile/get_watch_later_count get watch later classes count
     * @apiDescription Get user watch later classes count
     *
     * @apiName profile_get_watch_later_count
     * @apiGroup Profile
     *
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_watch_later_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watch_later_count($user_id = -1){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");


        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        //get watch later list
        $objects_list = UserWishlist::find()
            ->where("user_id=".$user_id)
            ->count();

        $items_count_obj = new \stdClass();
        $items_count_obj->items_count = intval($objects_list);

        return $items_count_obj;

    }

    /**
     * @api {get} /profile/get_watched_by_page get watched classes by page
     * @apiDescription Get user watched classes
     *
     * @apiName profile_get_watched_by_page
     * @apiGroup Profile
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "style,goals"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_watched_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watched_by_page($page=1, $full_list=0, $page_size=10, $user_id = -1)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($user_id, "user_id");



        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        //get watch later list
        $objects_list = UserWatched::find()->where("user_id=".$user_id)->all();
        $objects_id_array = [];
        foreach($objects_list as $obj){
            $objects_id_array[] = $obj->class_id;
        }


        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->limit($page_size*$page);
        }
        else{

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);

    }

    /**
     * @api {get} /profile/get_watched_count get watched classes count
     * @apiDescription Get user watched classes count
     *
     * @apiName profile_get_watched_count
     * @apiGroup Profile
     *
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_watched_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watched_count($user_id = -1){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");


        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        //get watch later list
        $objects_list = UserWatched::find()
            ->where("user_id=".$user_id)
            ->count();

        $items_count_obj = new \stdClass();
        $items_count_obj->items_count = intval($objects_list);

        return $items_count_obj;

    }



    /**
     * @api {get} /profile/get_favorite_by_page get favorite classes by page
     * @apiDescription Get user favorite classes
     *
     * @apiName profile_get_favorite_by_page
     * @apiGroup Profile
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "style,goals"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_favorite_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_favorite_by_page($page, $full_list, $page_size=10, $user_id = -1){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($user_id, "user_id");



        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        //get favorites list
        $objects_list = UserFavorites::find()->where("user_id=".$user_id)->all();
        $objects_id_array = [];
        foreach($objects_list as $obj){
            $objects_id_array[] = $obj->class_id;
        }


        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->limit($page_size*$page);
        }
        else{

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where(['in','id',$objects_id_array])
                ->orderBy("create_date DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /profile/get_favorite_count get favorite classes count
     * @apiDescription Get user favorite classes count
     *
     * @apiName profile_get_favorite_count
     * @apiGroup Profile
     *
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_favorite_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_favorite_count($user_id = -1){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");


        if ($user_id == -1){

            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        //get watch later list
        $objects_list = UserFavorites::find()
            ->where("user_id=".$user_id)
            ->count();

        $items_count_obj = new \stdClass();
        $items_count_obj->items_count = intval($objects_list);

        return $items_count_obj;



    }




    /**
     * @api {get} /profile/get_watch_stat_by_day get user statistic for watching classes by day
     * @apiDescription Get statistic for classes watching by day
     *
     * @apiName profile_get_watch_stat_by_day
     * @apiGroup Profile
     *
     * @apiParam {Number} day_count the days count for which you want to get statistics
     * @apiParam {Number} [user_id]  User ID for which you want to get the statistics. If this parameter is not
     * specified - the statistics will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "day_count": 10
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSuccessExample {json} Success-Example:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *           "date": "2016-07-13",
     *           "watch_seconds": 45
     *        }
     *     ]
     *
     * @apiSampleRequest /profile/get_watch_stat_by_day
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watch_stat_by_day($day_count,$user_id=-1)
    {

       //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($day_count, "day_count", true);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        if ($user_id == -1){

            //get current user
            $cur_user = User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }


        $summary = UserWatchSummary::find()
            ->where("date >= DATE_SUB(CURDATE(), INTERVAL ".$day_count." DAY) AND date <= CURDATE() AND user_id = ".$user_id)
            ->orderBy("date DESC")
            ->all();

        $ret_array = [];
        for ($i = 0; $i < $day_count; $i++)
        {
            $timestamp = time();
            $tm = 86400 * $i; // 60 * 60 * 24 = 86400 = 1 day in seconds
            $tm = $timestamp - $tm;

            $the_date = date("Y-m-d", $tm);

            $ret_obj = new \stdClass();

            $ret_obj->date = $the_date;
            $datetime = new \DateTime($the_date);
            $ret_obj->date_iso = $datetime->format(\DateTime::ATOM);

            foreach($summary as $sum){


                if ($sum->date == $the_date){
                    //$ret_obj->watch_minutes = round($sum->watch_seconds/60);
                    $ret_obj->watch_seconds = $sum->watch_seconds;
                }
            }
            if (!isset($ret_obj->watch_seconds)){
                $ret_obj->watch_seconds = 0;
            }




            $ret_array[] = $ret_obj;
        }


        return $ret_array;
    }


    /**
     * @api {get} /profile/get_watch_stat_by_week get user statistic for watching classes by week
     * @apiDescription Get statistic for classes watching by week
     *
     * @apiName profile_get_watch_stat_by_week
     * @apiGroup Profile
     *
     * @apiParam {Number} week_count the week count for which you want to get statistics
     * @apiParam {Number} [user_id]  User ID for which you want to get the statistics. If this parameter is not
     * specified - the statistics will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "week_count": 4
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSuccessExample {json} Success-Example:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *           "date": "2016-07-11",
     *           "watch_seconds": 187
     *        }
     *     ]
     *
     * @apiSampleRequest /profile/get_watch_stat_by_week
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watch_stat_by_week($week_count,$user_id=-1)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($week_count, "week_count", true);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        if ($user_id == -1){

            //get current user
            $cur_user = User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }

      //  $week_count--;


        $dateNow =  date("Y-m-d");
        $dateTimeNow = new \DateTime($dateNow);
        $week_begin = clone $dateTimeNow->modify('Monday -'.$week_count.' week');


        //get day count
        $date_from_time = strtotime($week_begin->format('Y-m-d'));
        $date_to_time = strtotime($dateNow);
        $datediff = $date_to_time - $date_from_time;
        $day_count =  floor($datediff/(60*60*24));


        $summary = UserWatchSummary::find()
            ->where("date >= DATE_SUB(CURDATE(), INTERVAL ".$day_count." DAY) AND date <= CURDATE() AND user_id = ".$user_id)
            ->orderBy("date DESC")
            ->all();

        $ret_array = [];
        $day_of_week = 0;
        $sum_of_week = 0;
        for ($i = 0; $i < $day_count+1; $i++)
        {


            $timestamp = $date_from_time;// time();
            $tm = 86400 * $i; // 60 * 60 * 24 = 86400 = 1 day in seconds
            $tm = $timestamp + $tm;

            $the_date = date("Y-m-d", $tm);

            if ($day_of_week == 0){
                $week_begin = $the_date;
                $sum_of_week = 0;

            }

            foreach($summary as $sum){


                if ($sum->date == $the_date){
                    $sum_of_week+=$sum->watch_seconds;
                }
            }

            if ($day_of_week == 6){


                $ret_obj = new \stdClass();
                $ret_obj->date = $week_begin;
                $datetime = new \DateTime($week_begin);
                $ret_obj->date_iso = $datetime->format(\DateTime::ATOM);

                $ret_obj->watch_seconds = $sum_of_week;

                $ret_array[] = $ret_obj;

                $day_of_week = -1;

            }
            elseif($i == $day_count){

                $ret_obj = new \stdClass();
                $ret_obj->date = $week_begin;
                $datetime = new \DateTime($week_begin);
                $ret_obj->date_iso = $datetime->format(\DateTime::ATOM);

                $ret_obj->watch_seconds = $sum_of_week;

                $ret_array[] = $ret_obj;

            }

            $day_of_week++;

        }

        return $ret_array;
    }


    /**
     * @api {get} /profile/get_watch_stat_by_month get user statistic for watching classes by month
     * @apiDescription Get statistic for classes watching by month
     *
     * @apiName profile_get_watch_stat_by_month
     * @apiGroup Profile
     *
     * @apiParam {Number} month_count the month count for which you want to get statistics
     * @apiParam {Number} [user_id]  User ID for which you want to get the statistics. If this parameter is not
     * specified - the statistics will be returned for the current user
     * @apiParamExample {json} Request-Example:
     *     {
     *       "month_count": 3
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSuccessExample {json} Success-Example:
     *     HTTP/1.1 200 OK
     *     [
     *       {
     *           "date": "2016-07-01",
     *           "watch_seconds": 187
     *        }
     *     ]
     *
     * @apiSampleRequest /profile/get_watch_stat_by_month
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_watch_stat_by_month($month_count,$user_id=-1)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($month_count, "month_count", true);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        if ($user_id == -1){

            //get current user
            $cur_user = User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }

        $month_count--;

        //get current user
        $access_token = $_GET["access-token"];
        $cur_user = User::findOne(['auth_key' => $access_token]);


        $dateNow =  date("Y-m-d");
        $dateTimeNow = new \DateTime($dateNow);
        $month_begin = clone $dateTimeNow->modify('-'.$month_count.' month');


        $month_begin_str = $month_begin->format('Y-m-d');
        $month_begin_str = substr($month_begin_str,0, -2);
        $month_begin_str = $month_begin_str."01";


        //get day count
        $date_from_time = strtotime($month_begin_str);
        $date_to_time = strtotime($dateNow);
        $datediff = $date_to_time - $date_from_time;
        $day_count =  floor($datediff/(60*60*24));


        $summary = UserWatchSummary::find()
            ->where("date >= DATE_SUB(CURDATE(), INTERVAL ".$day_count." DAY) AND date <= CURDATE() AND user_id = ".$user_id)
            ->orderBy("date DESC")
            ->all();


        $ret_array = [];
        $sum_of_month = 0;
        $cur_month  = date("m",$date_from_time);
        $month_begin = $month_begin_str;

        for ($i = 0; $i < $day_count+1; $i++)
        {


            $timestamp = $date_from_time;// time();
            $tm = 86400 * $i; // 60 * 60 * 24 = 86400 = 1 day in seconds
            $tm = $timestamp + $tm;

            $the_date = date("Y-m-d", $tm);
            $cur_month_new = date("m",$tm);


            foreach($summary as $sum){


                if ($sum->date == $the_date){
                    $sum_of_month+=$sum->watch_seconds;

                }
            }


            if ($cur_month_new != $cur_month){


                $ret_obj = new \stdClass();
                $ret_obj->date = $month_begin;
                $datetime = new \DateTime($month_begin);
                $ret_obj->date_iso = $datetime->format(\DateTime::ATOM);
                $ret_obj->watch_seconds = $sum_of_month;

                $ret_array[] = $ret_obj;

                $cur_month = $cur_month_new;
                $month_begin = $the_date;

                $sum_of_month = 0;

            }
            elseif($i == $day_count){

                $ret_obj = new \stdClass();
                $ret_obj->date = $month_begin;
                $datetime = new \DateTime($month_begin);
                $ret_obj->date_iso = $datetime->format(\DateTime::ATOM);
                $ret_obj->watch_seconds = $sum_of_month;

                $month_begin = $the_date;
                $ret_array[] = $ret_obj;

            }


        }

        return $ret_array;
    }


    /**
     * @api {get} /profile/get_billing_history get billing history
     * @apiDescription Get billing history
     *
     * @apiName profile_get_billing_history
     * @apiGroup Profile
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     *
     * @apiSampleRequest /profile/get_billing_history
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_billing_history(){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $cur_user = User::findOne(['auth_key' => $access_token]);

        $billing_history = BillingHistory::find()
            ->where("user_id=".$cur_user->id)
            ->orderBy("pay_date DESC")
            ->all();

        return $billing_history;

    }


    /**
     * @api {get} /profile/add_to_watch_later add to watch later
     * @apiDescription Add yoga class to watch later list
     *
     * @apiName profile_add_to_watch_later
     * @apiGroup Profile
     *
     * @apiParam {Number} class_id Yoga class id
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/add_to_watch_later
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionAdd_to_watch_later($class_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if wishlist exist
        $is_wishlist_yet = UserWishlist::find()
            ->where("user_id=".$cur_user->id." AND class_id=".$class_id)
            ->count();


        //check if class exist
        $class_exist = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id)
            ->count();

        if ($class_exist == 0){
            throw new YogaHttpException(404,"Yoga Class not found");
        }


        if ($is_wishlist_yet == 0){
            //create link
            $user_wishlist = new UserWishlist();
            $user_wishlist->user_id = $cur_user->id;
            $user_wishlist->class_id = $class_id;
            $user_wishlist->add_date = date("Y-m-d H:i:s");
            $user_wishlist->save();

            //!gamification
            //add to wish list
            //ToDo Check that user get karma only once for class
            $watched_class = YogaClasses::find()->andWhere("is_published=1")
                ->where("id=".$class_id)
                ->count();

            if ($watched_class > 0){

                //add karma
                $cur_user->profile->karma+=5;
                $cur_user->profile->save();
            }


        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already added this class to watch later list");
        }


        die;

    }


    /**
     * @api {get} /profile/remove_from_watch_later remove from watch later
     * @apiDescription Remove yoga class from watch later list
     *
     * @apiName profile_remove_from_watch_later
     * @apiGroup Profile
     *
     * @apiParam {Number} class_id Yoga class id
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/remove_from_watch_later
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionRemove_from_watch_later($class_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if wishlist exist
        $is_wishlist_yet = UserWishlist::find()
            ->where("user_id=".$cur_user->id." AND class_id=".$class_id)
            ->one();


        //check if class exist
        $class_exist = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id)
            ->count();

        if ($class_exist == 0){
            throw new YogaHttpException(404,"Yoga Class not found");
        }



        if (isset($is_wishlist_yet)){
            $is_wishlist_yet->delete();
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already removed this class from watch later list");
        }

        die;

    }


    /**
     * @api {get} /profile/add_to_favorite add to favorite
     * @apiDescription Add yoga class to favorite list
     *
     * @apiName profile_add_to_favorite
     * @apiGroup Profile
     *
     * @apiParam {Number} class_id Yoga class id
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/add_to_favorite
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionAdd_to_favorite($class_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if wishlist exist
        $is_added_yet = UserFavorites::find()
            ->where("user_id=".$cur_user->id." AND class_id=".$class_id)
            ->count();


        //check if class exist
        $class_exist = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id)
            ->count();

        if ($class_exist == 0){
            throw new YogaHttpException(404,"Yoga Class not found");
        }


        if ($is_added_yet == 0){
            //create link
            $user_add = new UserFavorites();
            $user_add->user_id = $cur_user->id;
            $user_add->class_id = $class_id;
            $user_add->save();

        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already added this class to favorite list");
        }

        die;

    }


    /**
     * @api {get} /profile/remove_from_favorite remove from favorite
     * @apiDescription Remove yoga class from favorite list
     *
     * @apiName profile_remove_from_favorite
     * @apiGroup Profile
     *
     * @apiParam {Number} class_id Yoga class id
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/remove_from_favorite
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionRemove_from_favorite($class_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if wishlist exist
        $is_wishlist_yet = UserFavorites::find()
            ->where("user_id=".$cur_user->id." AND class_id=".$class_id)
            ->one();


        //check if class exist
        $class_exist = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id)
            ->count();

        if ($class_exist == 0){
            throw new YogaHttpException(404,"Yoga Class not found");
        }



        if (isset($is_wishlist_yet)){
            $is_wishlist_yet->delete();
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already removed this class from favorite list");
        }

        die;

    }


    /**
     * @api {get} /profile/get_karma_level get karma level
     * @apiDescription Get level for karma amount
     *
     * @apiName profile_get_karma_level
     * @apiGroup Profile
     *
     * @apiParam {Number} karma karma amount
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     *
     * * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "karma_level": "Expert"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/get_karma_level
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_karma_level(){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $karma = App::getRequestData("karma",false);

        YogaHttpException::checkIntParam($karma,"karma",true);


        $karma_levels = UserKarmaLevel::find()->all();

        $karma_level = "undefined";
        foreach($karma_levels as $level) {
            if ($karma >= $level->min && $karma <= $level->max) {
                $karma_level = $level->title;
            }
        }

        $return_object = new \stdClass();
        $return_object->karma_level = $karma_level;
        return $return_object;
    }



    /**
     * @api {get} /profile/get_all_karma_levels get all karma levels
     * @apiDescription Get all karma levels
     *
     * @apiName profile_get_all_karma_levels
     * @apiGroup Profile
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     *
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     *
     * @apiSampleRequest /profile/get_all_karma_levels
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_all_karma_levels(){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $cur_user = User::findOne(['auth_key' => $access_token]);

        $karma_levels = UserKarmaLevel::find()->orderBy(['min'=>SORT_ASC,])->all();


        $return_array = [];
        foreach($karma_levels as $level){

            $karma_level = new \stdClass();
            $karma_level->title = $level->title;
            $karma_level->min = $level->min;
            $karma_level->max = $level->max;
            $return_array[] = $karma_level;
        }
        return $return_array;

    }


    /**
     * @api {get} /profile/get_user_activities_by_page get user activities by page
     * @apiDescription Get user activities by page
     *
     * @apiName get_user_activities_by_page
     * @apiGroup Profile
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [user_id]  User ID for which you want to get the objects. If this parameter is not
     * specified - the objects will be returned for the current user
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /profile/get_user_activities_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_user_activities_by_page($page, $full_list, $page_size=10, $user_id = -1){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($user_id, "user_id");



        if ($user_id == -1){
            $cur_user =  User::findOne(['auth_key' => $access_token]);
            $user_id = $cur_user->id;
        }



        if ($full_list){

            $stat_list = UserWatchStat::find()
                ->where("user_id = ".$user_id)
                ->andWhere("(`watch_end` - `watch_begin`) <> 0")
                ->orderBy("watch_date DESC")
                ->limit($page_size*$page);
        }
        else{

            $stat_list = UserWatchStat::find()
                ->where("user_id = ".$user_id)
                ->andWhere("(`watch_end` - `watch_begin`) <> 0")
                ->orderBy("watch_date DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $stat_list,
            'pagination' => false,
        ]);

    }






  
}