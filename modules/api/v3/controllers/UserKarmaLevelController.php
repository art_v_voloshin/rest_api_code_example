<?php
namespace app\modules\api\v3\controllers;



use app\models\UserKarmaLevel;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use yii\rest\ActiveController;
use yii\web\Response;


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');


class UserKarmaLevelController extends ActiveController
{

    public $modelClass = 'app\models\UserKarmaLevel';

}