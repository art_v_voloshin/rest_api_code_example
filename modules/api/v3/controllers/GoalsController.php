<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\Goals;
use yii\data\ActiveDataProvider;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');


class GoalsController extends ActiveController
{

    public $modelClass = 'app\models\Goals';


    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {


        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @api {get} /goals get all goals by page
     * @apiDescription Get all goals
     *
     * @apiName goals
     * @apiGroup Goals
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /goals
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $objects_list = Goals::find()
                ->orderBy("id desc")
                ->limit($page_size*$page);
        }
        else{

            $objects_list = Goals::find()
                ->orderBy("id desc")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /goals/get_by_id get goal by id
     * @apiDescription Get goal by id
     *
     * @apiName goals_get_by_id
     * @apiGroup Goals
     *
     * @apiParam {Number} goal_id  Goal id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "goal_id": 5
     *     }
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /goals/get_by_id
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_by_id($goal_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($goal_id, "goal_id", true);

        $objects_list = Goals::find()
            ->where("id=".$goal_id);

        $badge_count = Goals::find()
            ->where("id=".$goal_id)
            ->count();

        if ($badge_count == 0){
            throw new YogaHttpException(404,"Goal not found");
        }


        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }





}