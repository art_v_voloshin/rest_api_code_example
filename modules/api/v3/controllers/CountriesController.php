<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\Country;
use yii\data\ActiveDataProvider;
use yii\filters\auth\QueryParamAuth;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');


class CountriesController extends ActiveController
{ 

    public $modelClass = 'app\models\Country';

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {


        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @api {get} /countries get all countries by page
     * @apiDescription Get all countries
     *
     * @apiName countries
     * @apiGroup Countries
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /countries
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $object_list = Country::find()
                ->orderBy("id desc")
                ->limit($page_size*$page);
        }
        else{

            $object_list = Country::find()
                ->orderBy("id desc")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);

    }




}