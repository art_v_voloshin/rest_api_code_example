<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\Badges;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');


class BadgesController extends ActiveController
{

    public $modelClass = 'app\models\Badges';


    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {


        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @api {get} /badges get all badges by page
     * @apiDescription Get all badges
     *
     * @apiName badges
     * @apiGroup Badges
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /badges
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $objects_list = Badges::find()
                ->orderBy("id desc")
                ->limit($page_size*$page);
        }
        else{

            $objects_list = Badges::find()
                ->orderBy("id desc")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /badges/get_by_id get badge by id
     * @apiDescription Get badge by id
     *
     * @apiName badges_get_by_id
     * @apiGroup Badges
     *
     * @apiParam {Number} badge_id  Badge id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "badge_id": 16
     *     }
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /badges/get_by_id
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_by_id($badge_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($badge_id, "badge_id", true);

        $objects_list = Badges::find()
            ->where("id=".$badge_id);

        $badge_count = $objects_list
            ->count();

        if ($badge_count == 0){
            throw new YogaHttpException(404,"Badge not found");
        }


        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }
}
